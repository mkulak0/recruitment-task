<?php 

namespace App\Service;

class EmailService {
    static function validation($string) : bool
    {
        return (filter_var($string, FILTER_VALIDATE_EMAIL) ? true : false);
    }
}