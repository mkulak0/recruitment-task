<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Doctrine\ORM\EntityManagerInterface;

use App\Entity\Email;
use App\Model\Pet;
use App\Service\EmailService;

class EmailController extends AbstractController
{
    // #[Route('/sendEmail', name: 'send_email', methods:['POST'])]
    public function send(Request $request, EntityManagerInterface $entityManager, EmailService $emailService) : Response
    {
        $response = new Response();
        /* Checking requriments */
        if(
            $request->request->has('name') &&
            $request->request->has('species') &&
            $request->request->has('email')
        ){
            if($emailService->validation($request->request->get('email'))){
                /* Object with random info about pet */
                $response_object = json_decode(file_get_contents('https://cat-fact.herokuapp.com/facts/random'));
    
                /* Pet object contain user+api infos */
                $pet = new Pet($response_object, $request->request->get('name'));
    
    
                $email = new Email();
                /* $pet object cast to string */
                $email->setContent($pet);
    
                /* Saving to DB */
                $entityManager->persist($email);
                $entityManager->flush();
    
    
                mail($request->request->get('email'), "Raport na temat zwierzęcia ".$pet->getName(), $pet);
                $response->setContent("OK");

            } else {
                $response->setStatusCode(Response::HTTP_BAD_REQUEST);
                // Invalid email
                $response->setContent("Invalid email");
            }
        } else {
            // Lack of information
            $response->setStatusCode(Response::HTTP_BAD_REQUEST);
            $response->setContent("Lack of information. name, species and email fields are required.");
        }

        return $response;
    }
}