<?php

namespace App\Model;

class Pet {
    /* From API */
    private $_id;
    private $status;
    private $type;
    private $deleted;
    private $user;
    private $text;

    /* From user */
    private $name;


    public function __construct($obj, $name){
        $this->_id = $obj->_id;
        $this->status = $obj->status;
        $this->type = $obj->type;
        $this->deleted = $obj->deleted;
        $this->user = $obj->user;
        $this->text = $obj->text;
        $this->name = $name;
    }

    public function __toString(){
        $str_status = "Raport na temat zwierzęcia \n";
        $str_status .= "Zweryfikowany: ".($this->status->verified !== null ? "Tak" : "Nie")."\n"; 
        $str_status .= "Gatunek: ".$this->type."\n";
        $str_status .= "Czy usunięty (?): ".($this->deleted ? "Tak" : "Nie")."\n";
        $str_status .= "ID właściciela (?): ".$this->user."\n";
        $str_status .= "Tekst: ".$this->text."\n";
        $str_status .= "Imię: ".$this->name;
        return $str_status;
    }

    public function getName() : string
    {
        return $this->name;
    }
}